#!/usr/bin/env python
# encoding=utf8
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# made by zodman
import bcvc
import ouoio
import shinkin
import shorte
import uskipme
import adfly
import pycountry
import pprint
from countryinfo import countries
import sys
import click
import json

def search_countryinfo(code):
    for i in countries:
        if code == i["code"]:
            return i

def cleanup_decimal(number):
    number= number.replace(",",".")
    number = number.replace("$","")
    return number

def generate_json(results):
    p = {}
    for r in results:
        p.setdefault(r["code"],[])
        p[r["code"]].append(r)

    open("out.json","w").write(json.dumps(p))

@click.command()
@click.option("--filtercontinent")
def main(filtercontinent):
    big_dict = []
    for i in (bcvc, ouoio, shinkin, shorte, uskipme, adfly):
        module_name =  i.__name__
        for country,cc_mobile, cc in i.scrape_rates():
            try:
                result = pycountry.countries.lookup(country)
                code = result.alpha_2
                res2 = search_countryinfo(code)
                if res2:
                    continent = res2["continent"]
                else:
                    continent = None
            except LookupError:
                continent = None

            d = dict(module=module_name, country=country, cc=cleanup_decimal(cc), 
                    continent=continent, code=code)
            big_dict.append(d)
    tb=[]
    results =  sorted(big_dict, key=lambda d:d["cc"], reverse=True)
    generate_json(results)
    for i in results:
        cont = i["continent"] or ""
        if filtercontinent is None:
            tb.append([ i['cc'],i["code"], i['country'].decode("ascii","ignore"), 
                        i["module"], i["continent"] ])
        elif filtercontinent.lower() in cont.lower():
            tb.append([ i['cc'], i["code"],i['country'].decode("ascii","ignore"), 
                        i["module"], i["continent"] ])

    from tabulate import tabulate
    print tabulate(tb, headers=["Precio", "code", "Pais","Acortador", "continent"], tablefmt="pipe")

if __name__ == "__main__":
    main()
