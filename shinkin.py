from webscraping import download, xpath

URL = "https://panel.shink.in/rates"

def scrape_rates():
    D = download.Download()
    response = D.get(URL)
    tds = xpath.search(response,"//table/tbody/tr/td")
    countries = []
    for i in xrange(0,len(tds)-1,2):
        countries.append((tds[i].lower(),None, tds[i+1]))
    return countries

if __name__== "__main__":
    print scrape_rates()
