|   Precio | code   | Pais                                         | Acortador   | continent     |
|---------:|:-------|:---------------------------------------------|:------------|:--------------|
|     9.2  | DK     | denmark                                      | shorte      | Europe        |
|     8.61 | AU     | australia                                    | shorte      | Oceania       |
|     8.18 | CO     | noruega                                      | shorte      |               |
|     8.11 | KW     | kuwait                                       | uskipme     | Asia          |
|     7.72 | AE     | united arab emirates                         | shorte      | Asia          |
|     7.16 | CO     | alemania                                     | shorte      |               |
|     7.06 | FI     | finland                                      | shorte      | Europe        |
|     6.97 | ZA     | south africa                                 | shorte      | Africa        |
|     6.93 | SE     | sweden                                       | shorte      | Europe        |
|     6.21 | NZ     | new zealand                                  | shorte      | Oceania       |
|     6.01 | CH     | switzerland                                  | shorte      | Europe        |
|     5.99 | NL     | netherlands                                  | shorte      | Europe        |
|     5.81 | EG     | canad                                        | shorte      |               |
|     5.79 | IE     | ireland                                      | shorte      | Europe        |
|     5.24 | QA     | qatar                                        | shorte      | Asia          |
|     5.16 | SG     | singapore                                    | shorte      | Asia          |
|     5    | SE     | Sweden                                       | bcvc        | Europe        |
|     5    | FI     | Finland                                      | bcvc        | Europe        |
|     5    | DK     | Denmark                                      | bcvc        | Europe        |
|     5    | US     | United States                                | bcvc        | North America |
|     4.86 | FR     | france                                       | shorte      | Europe        |
|     4.7  | AU     | australia                                    | ouoio       | Oceania       |
|     4.67 | BE     | belgium                                      | shorte      | Europe        |
|     4.65 | AU     | australia                                    | shinkin     | Oceania       |
|     4.5  | FR     | France                                       | bcvc        | Europe        |
|     4.3  | GB     | united kingdom                               | ouoio       | Europe        |
|     4.29 | GB     | united kingdom                               | shinkin     | Europe        |
|     4.19 | CO     | polonia                                      | shorte      |               |
|     4.15 | QA     | qatar                                        | uskipme     | Asia          |
|     4.1  | US     | united states                                | ouoio       | North America |
|     4.05 | US     | united states                                | shinkin     | North America |
|     4.05 | NO     | norway                                       | shinkin     | Europe        |
|     4    | AU     | Australia                                    | bcvc        | Oceania       |
|     4    | AE     | United Arab Emirates                         | bcvc        | Asia          |
|     4    | AT     | Austria                                      | bcvc        | Europe        |
|     4    | GB     | United Kingdom                               | bcvc        | Europe        |
|     4    | CH     | Switzerland                                  | bcvc        | Europe        |
|     4    | ZA     | South Africa                                 | bcvc        | Africa        |
|     4    | KW     | Kuwait                                       | bcvc        | Asia          |
|     4    | DE     | Germany                                      | bcvc        | Europe        |
|     4    | BH     | Bahrain                                      | bcvc        | Asia          |
|     4    | JO     | Jordan                                       | bcvc        | Asia          |
|     4    | QA     | Qatar                                        | bcvc        | Asia          |
|     4    | OM     | Oman                                         | bcvc        | Asia          |
|     4    | NO     | Norway                                       | bcvc        | Europe        |
|     4    | IQ     | Iraq                                         | bcvc        | Asia          |
|     4    | IE     | Ireland                                      | bcvc        | Europe        |
|     4    | NO     | norway                                       | ouoio       | Europe        |
|     3.61 | IS     | iceland                                      | shorte      | Europe        |
|     3.59 | IT     | italy                                        | shorte      | Europe        |
|     3.54 | AT     | austria                                      | shorte      | Europe        |
|     3.5  | PL     | Poland                                       | bcvc        | Europe        |
|     3.5  | AU     | espaa                                        | shorte      |               |
|     3.5  | US     | united states                                | uskipme     | North America |
|     3.43 | IL     | israel                                       | shorte      | Asia          |
|     3.41 | SE     | sweden                                       | shinkin     | Europe        |
|     3.41 | OM     | oman                                         | uskipme     | Asia          |
|     3.41 | AE     | united arab emirates                         | uskipme     | Asia          |
|     3.36 | SE     | sweden                                       | ouoio       | Europe        |
|     3.35 | CA     | canada                                       | shinkin     | North America |
|     3.27 | CA     | canada                                       | ouoio       | North America |
|     3.21 | BH     | bahrain                                      | uskipme     | Asia          |
|     3.2  | CA     | Canada                                       | bcvc        | North America |
|     3.1  | SA     | saudi arabia                                 | uskipme     | Asia          |
|     3.1  | NO     | norway                                       | uskipme     | Europe        |
|     3    | SG     | Singapore                                    | bcvc        | Asia          |
|     3    | NL     | Netherlands                                  | bcvc        | Europe        |
|     3    | LB     | Lebanon                                      | bcvc        | Asia          |
|     3    | NZ     | New Zealand                                  | bcvc        | Oceania       |
|     2.95 | GB     | united kingdom                               | uskipme     | Europe        |
|     2.93 | CO     | arabia saud                                  | shorte      |               |
|     2.8  | NG     | nigeria                                      | uskipme     | Africa        |
|     2.76 | LB     | lebanon                                      | shorte      | Asia          |
|     2.74 | CY     | cyprus                                       | shorte      | Asia          |
|     2.73 | FO     | faroe islands                                | shorte      |               |
|     2.6  | LV     | latvia                                       | shorte      | Europe        |
|     2.6  | KW     | kuwait                                       | shorte      | Asia          |
|     2.5  | BE     | Belgium                                      | bcvc        | Europe        |
|     2.5  | TR     | Turkey                                       | bcvc        | Asia          |
|     2.5  | WS     | Samoa                                        | bcvc        | Oceania       |
|     2.5  | AS     | American Samoa                               | bcvc        |               |
|     2.5  | AU     | australia                                    | uskipme     | Oceania       |
|     2.5  | ZA     | south africa                                 | uskipme     | Africa        |
|     2.48 | SK     | slovakia                                     | uskipme     | Europe        |
|     2.45 | IQ     | iraq                                         | uskipme     | Asia          |
|     2.45 | DK     | denmark                                      | uskipme     | Europe        |
|     2.45 | FI     | finland                                      | uskipme     | Europe        |
|     2.42 | SI     | slovenia                                     | shorte      | Europe        |
|     2.42 | PT     | portugal                                     | shorte      | Europe        |
|     2.42 | CZ     | czech republic                               | shorte      | Europe        |
|     2.4  | OM     | oman                                         | shorte      | Asia          |
|     2.24 | SK     | slovakia                                     | shorte      | Europe        |
|     2.24 | NG     | hong kong sar china                          | shorte      |               |
|     2.13 | LU     | luxembourg                                   | shorte      | Europe        |
|     2.11 | EE     | estonia                                      | shorte      | Europe        |
|     2.1  | SI     | slovenia                                     | uskipme     | Europe        |
|     2.1  | IS     | iceland                                      | uskipme     | Europe        |
|     2.1  | LV     | latvia                                       | uskipme     | Europe        |
|     2.08 | BH     | bahrain                                      | shorte      | Asia          |
|     2.02 | IQ     | iraq                                         | shorte      | Asia          |
|     2    | GR     | Greece                                       | bcvc        | Europe        |
|     2    | AZ     | Azerbaijan                                   | bcvc        | Asia          |
|     2    | ES     | Spain                                        | bcvc        | Europe        |
|     2    | IT     | Italy                                        | bcvc        | Europe        |
|     2    | SV     | El Salvador                                  | bcvc        | North America |
|     2    | CZ     | Czech Republic                               | bcvc        | Europe        |
|     2    | CZ     | Slovakia (Slovak Republic)                   | bcvc        |               |
|     2    | PY     | Paraguay                                     | bcvc        | South America |
|     2    | HU     | Hungary                                      | bcvc        | Europe        |
|     2    | LT     | Lithuania                                    | bcvc        | Europe        |
|     2    | LU     | Luxembourg                                   | bcvc        | Europe        |
|     2    | SD     | sudan                                        | uskipme     | Africa        |
|     2    | IE     | ireland                                      | uskipme     | Europe        |
|     2    | NZ     | new zealand                                  | uskipme     | Oceania       |
|     2    | LU     | luxembourg                                   | uskipme     | Europe        |
|     2    | CY     | cyprus                                       | uskipme     | Asia          |
|     2    | LB     | lebanon                                      | uskipme     | Asia          |
|    14.04 | EG     | estados unidos                               | shorte      |               |
|    10.75 | EG     | reino unido                                  | shorte      |               |
|     1.95 | BE     | belgium                                      | shinkin     | Europe        |
|     1.94 | SG     | singapore                                    | shinkin     | Asia          |
|     1.88 | ZA     | south africa                                 | ouoio       | Africa        |
|     1.88 | FI     | finland                                      | ouoio       | Europe        |
|     1.88 | ZA     | south africa                                 | shinkin     | Africa        |
|     1.88 | FI     | finland                                      | shinkin     | Europe        |
|     1.83 | PL     | poland                                       | ouoio       | Europe        |
|     1.83 | MY     | malaysia                                     | shorte      | Asia          |
|     1.8  | IS     | Iceland                                      | bcvc        | Europe        |
|     1.8  | SE     | sweden                                       | uskipme     | Europe        |
|     1.78 | PL     | poland                                       | shinkin     | Europe        |
|     1.76 | JP     | japan                                        | shorte      | Asia          |
|     1.76 | GE     | georgia                                      | shorte      | Asia          |
|     1.7  | ES     | spain                                        | shinkin     | Europe        |
|     1.7  | NG     | nigeria                                      | shorte      | Africa        |
|     1.7  | ES     | spain                                        | ouoio       | Europe        |
|     1.7  | DE     | germany                                      | uskipme     | Europe        |
|     1.7  | EE     | estonia                                      | uskipme     | Europe        |
|     1.7  | CA     | canada                                       | uskipme     | North America |
|     1.7  | IN     | india                                        | uskipme     | Asia          |
|     1.68 | PG     | papua new guinea                             | shorte      | Oceania       |
|     1.65 | IE     | ireland                                      | ouoio       | Europe        |
|     1.65 | NZ     | new zealand                                  | ouoio       | Oceania       |
|     1.65 | NL     | netherlands                                  | ouoio       | Europe        |
|     1.65 | BR     | brazil                                       | ouoio       | South America |
|     1.65 | IE     | ireland                                      | shinkin     | Europe        |
|     1.65 | BR     | brazil                                       | shinkin     | South America |
|     1.65 | NL     | netherlands                                  | shinkin     | Europe        |
|     1.65 | NZ     | new zealand                                  | shinkin     | Oceania       |
|     1.64 | BG     | bulgaria                                     | shorte      | Europe        |
|     1.6  | PA     | panama                                       | uskipme     | North America |
|     1.6  | KE     | kenya                                        | uskipme     | Africa        |
|     1.59 | IN     | india                                        | shorte      | Asia          |
|     1.55 | FO     | russia                                       | shorte      |               |
|     1.55 | TR     | turkey                                       | shorte      | Asia          |
|     1.53 | AF     | afghanistan                                  | shorte      | Asia          |
|     1.52 | MP     | northern mariana islands                     | shorte      |               |
|     1.51 | MS     | Montserrat                                   | bcvc        |               |
|     1.51 | SB     | solomon islands                              | shorte      | Oceania       |
|     1.51 | TH     | thailand                                     | shorte      | Asia          |
|     1.5  | PT     | Portugal                                     | bcvc        | Europe        |
|     1.5  | RU     | Russian Federation                           | bcvc        | Europe        |
|     1.5  | SI     | Slovenia                                     | bcvc        | Europe        |
|     1.5  | VE     | Venezuela                                    | bcvc        | South America |
|     1.5  | PG     | Papua New Guinea                             | bcvc        | Oceania       |
|     1.5  | PE     | peru                                         | shinkin     | South America |
|     1.5  | PE     | reunion                                      | shinkin     |               |
|     1.5  | RO     | romania                                      | shinkin     | Europe        |
|     1.5  | OM     | oman                                         | shinkin     | Asia          |
|     1.5  | OM     | russia                                       | shinkin     |               |
|     1.5  | RW     | rwanda                                       | shinkin     | Africa        |
|     1.5  | MP     | northern mariana islands                     | shinkin     |               |
|     1.5  | MP     | north korea                                  | shinkin     |               |
|     1.5  | MP     | saint barthelemy                             | shinkin     |               |
|     1.5  | MP     | saint helena                                 | shinkin     |               |
|     1.5  | PK     | pakistan                                     | shinkin     | Asia          |
|     1.5  | QA     | qatar                                        | shinkin     | Asia          |
|     1.5  | QA     | phillipines                                  | shinkin     |               |
|     1.5  | PN     | pitcairn                                     | shinkin     |               |
|     1.5  | PN     | south korea                                  | shinkin     |               |
|     1.5  | PG     | papua new guinea                             | shinkin     | Oceania       |
|     1.5  | PT     | portugal                                     | shinkin     | Europe        |
|     1.5  | PA     | panama                                       | shinkin     | North America |
|     1.5  | PA     | palestine                                    | shinkin     |               |
|     1.5  | PR     | puerto rico                                  | shinkin     |               |
|     1.5  | PW     | palau                                        | shinkin     | Oceania       |
|     1.5  | PY     | paraguay                                     | shinkin     | South America |
|     1.5  | NF     | norfolk island                               | shinkin     |               |
|     1.5  | NU     | niue                                         | shinkin     |               |
|     1.5  | NU     | micronesia                                   | shinkin     |               |
|     1.5  | MX     | mexico                                       | shinkin     | North America |
|     1.5  | YT     | mayotte                                      | shinkin     |               |
|     1.5  | MU     | mauritius                                    | shinkin     | Africa        |
|     1.5  | MR     | mauritania                                   | shinkin     | Africa        |
|     1.5  | MQ     | martinique                                   | shinkin     |               |
|     1.5  | MH     | marshall islands                             | shinkin     | Oceania       |
|     1.5  | MT     | malta                                        | shinkin     | Europe        |
|     1.5  | ML     | mali                                         | shinkin     | Africa        |
|     1.5  | MV     | maldives                                     | shinkin     | Asia          |
|     1.5  | MY     | malaysia                                     | shinkin     | Asia          |
|     1.5  | MW     | malawi                                       | shinkin     | Africa        |
|     1.5  | MG     | madagascar                                   | shinkin     | Africa        |
|     1.5  | MG     | moldava                                      | shinkin     |               |
|     1.5  | MC     | monaco                                       | shinkin     | Europe        |
|     1.5  | MN     | mongolia                                     | shinkin     | Asia          |
|     1.5  | NG     | nigeria                                      | shinkin     | Africa        |
|     1.5  | NE     | niger                                        | shinkin     | Africa        |
|     1.5  | NI     | nicaragua                                    | shinkin     | North America |
|     1.5  | GY     | guyana                                       | shinkin     | South America |
|     1.5  | NC     | new caledonia                                | shinkin     |               |
|     1.5  | NP     | nepal                                        | shinkin     | Asia          |
|     1.5  | NR     | nauru                                        | shinkin     | Oceania       |
|     1.5  | NA     | namibia                                      | shinkin     | Africa        |
|     1.5  | NA     | myanmar (burma)                              | shinkin     |               |
|     1.5  | MZ     | mozambique                                   | shinkin     | Africa        |
|     1.5  | MA     | morocco                                      | shinkin     | Africa        |
|     1.5  | MS     | montserrat                                   | shinkin     |               |
|     1.5  | ME     | montenegro                                   | shinkin     | Europe        |
|     1.5  | ME     | macedonia                                    | shinkin     |               |
|     1.5  | ME     | anonymous                                    | shinkin     |               |
|     1.5  | UA     | ukraine                                      | shinkin     | Europe        |
|     1.5  | UG     | uganda                                       | shinkin     | Africa        |
|     1.5  | TV     | tuvalu                                       | shinkin     | Oceania       |
|     1.5  | TC     | turks and caicos islands                     | shinkin     |               |
|     1.5  | TM     | turkmenistan                                 | shinkin     | Asia          |
|     1.5  | TR     | turkey                                       | shinkin     | Asia          |
|     1.5  | TN     | tunisia                                      | shinkin     | Africa        |
|     1.5  | TT     | trinidad and tobago                          | shinkin     | North America |
|     1.5  | TO     | tonga                                        | shinkin     | Oceania       |
|     1.5  | TK     | tokelau                                      | shinkin     |               |
|     1.5  | TG     | togo                                         | shinkin     | Africa        |
|     1.5  | TG     | timor-leste (east timor)                     | shinkin     |               |
|     1.5  | AE     | united arab emirates                         | shinkin     | Asia          |
|     1.5  | UM     | united states minor outlying islands         | shinkin     |               |
|     1.5  | UY     | uruguay                                      | shinkin     | South America |
|     1.5  | ZW     | zimbabwe                                     | shinkin     | Africa        |
|     1.5  | ZM     | zambia                                       | shinkin     | Africa        |
|     1.5  | YE     | yemen                                        | shinkin     | Asia          |
|     1.5  | EH     | western sahara                               | shinkin     |               |
|     1.5  | WF     | wallis and futuna                            | shinkin     |               |
|     1.5  | WF     | virgin islands, us                           | shinkin     |               |
|     1.5  | VG     | virgin islands, british                      | shinkin     |               |
|     1.5  | VN     | vietnam                                      | shinkin     | Asia          |
|     1.5  | VE     | venezuela                                    | shinkin     | South America |
|     1.5  | VE     | vatican city                                 | shinkin     |               |
|     1.5  | VU     | vanuatu                                      | shinkin     | Oceania       |
|     1.5  | UZ     | uzbekistan                                   | shinkin     | Asia          |
|     1.5  | TH     | thailand                                     | shinkin     | Asia          |
|     1.5  | TZ     | tanzania                                     | shinkin     | Africa        |
|     1.5  | TJ     | tajikistan                                   | shinkin     | Asia          |
|     1.5  | SL     | sierra leone                                 | shinkin     | Africa        |
|     1.5  | SC     | seychelles                                   | shinkin     | Africa        |
|     1.5  | RS     | serbia                                       | shinkin     | Europe        |
|     1.5  | SN     | senegal                                      | shinkin     | Africa        |
|     1.5  | SA     | saudi arabia                                 | shinkin     | Asia          |
|     1.5  | ST     | sao tome and principe                        | shinkin     | Africa        |
|     1.5  | SM     | san marino                                   | shinkin     | Europe        |
|     1.5  | WS     | samoa                                        | shinkin     | Oceania       |
|     1.5  | VC     | saint vincent and the grenadines             | shinkin     | North America |
|     1.5  | PM     | saint pierre and miquelon                    | shinkin     |               |
|     1.5  | PM     | saint martin                                 | shinkin     |               |
|     1.5  | LC     | saint lucia                                  | shinkin     | North America |
|     1.5  | LC     | sint maarten                                 | shinkin     |               |
|     1.5  | SK     | slovakia                                     | shinkin     | Europe        |
|     1.5  | SI     | slovenia                                     | shinkin     | Europe        |
|     1.5  | TW     | taiwan                                       | shinkin     |               |
|     1.5  | TW     | syria                                        | shinkin     |               |
|     1.5  | CH     | switzerland                                  | shinkin     | Europe        |
|     1.5  | SZ     | swaziland                                    | shinkin     | Africa        |
|     1.5  | SJ     | svalbard and jan mayen                       | shinkin     |               |
|     1.5  | SR     | suriname                                     | shinkin     | South America |
|     1.5  | SD     | sudan                                        | shinkin     | Africa        |
|     1.5  | LK     | sri lanka                                    | shinkin     | Asia          |
|     1.5  | SS     | south sudan                                  | shinkin     |               |
|     1.5  | GS     | south georgia and the south sandwich islands | shinkin     |               |
|     1.5  | SO     | somalia                                      | shinkin     | Africa        |
|     1.5  | SB     | solomon islands                              | shinkin     | Oceania       |
|     1.5  | KN     | saint kitts and nevis                        | shinkin     | North America |
|     1.5  | EC     | ecuador                                      | shinkin     | South America |
|     1.5  | CO     | colombia                                     | shinkin     | South America |
|     1.5  | CC     | cocos (keeling) islands                      | shinkin     |               |
|     1.5  | CX     | christmas island                             | shinkin     |               |
|     1.5  | CN     | china                                        | shinkin     | Asia          |
|     1.5  | CL     | chile                                        | shinkin     | South America |
|     1.5  | TD     | chad                                         | shinkin     | Africa        |
|     1.5  | CF     | central african republic                     | shinkin     | Africa        |
|     1.5  | KY     | cayman islands                               | shinkin     |               |
|     1.5  | KY     | cape verde                                   | shinkin     |               |
|     1.5  | CM     | cameroon                                     | shinkin     | Africa        |
|     1.5  | KH     | cambodia                                     | shinkin     | Asia          |
|     1.5  | BI     | burundi                                      | shinkin     | Africa        |
|     1.5  | BF     | burkina faso                                 | shinkin     | Africa        |
|     1.5  | KM     | comoros                                      | shinkin     | Africa        |
|     1.5  | CG     | congo                                        | shinkin     | Africa        |
|     1.5  | DO     | dominican republic                           | shinkin     | North America |
|     1.5  | DM     | dominica                                     | shinkin     | North America |
|     1.5  | DJ     | djibouti                                     | shinkin     | Africa        |
|     1.5  | DK     | denmark                                      | shinkin     | Europe        |
|     1.5  | DK     | democratic republic of the congo             | shinkin     |               |
|     1.5  | CZ     | czech republic                               | shinkin     | Europe        |
|     1.5  | CY     | cyprus                                       | shinkin     | Asia          |
|     1.5  | CY     | curacao                                      | shinkin     |               |
|     1.5  | CU     | cuba                                         | shinkin     | North America |
|     1.5  | HR     | croatia                                      | shinkin     | Europe        |
|     1.5  | HR     | cote d'ivoire (ivory coast)                  | shinkin     |               |
|     1.5  | CR     | costa rica                                   | shinkin     | North America |
|     1.5  | CK     | cook islands                                 | shinkin     |               |
|     1.5  | BG     | bulgaria                                     | shinkin     | Europe        |
|     1.5  | BG     | brunei                                       | shinkin     |               |
|     1.5  | IO     | british indian ocean territory               | shinkin     |               |
|     1.5  | AZ     | azerbaijan                                   | shinkin     | Asia          |
|     1.5  | AT     | austria                                      | shinkin     | Europe        |
|     1.5  | AW     | aruba                                        | shinkin     |               |
|     1.5  | AM     | armenia                                      | shinkin     | Asia          |
|     1.5  | AR     | argentina                                    | shinkin     | South America |
|     1.5  | AG     | antigua and barbuda                          | shinkin     | North America |
|     1.5  | AQ     | antarctica                                   | shinkin     |               |
|     1.5  | AI     | anguilla                                     | shinkin     |               |
|     1.5  | AO     | angola                                       | shinkin     | Africa        |
|     1.5  | AD     | andorra                                      | shinkin     | Europe        |
|     1.5  | AS     | american samoa                               | shinkin     |               |
|     1.5  | DZ     | algeria                                      | shinkin     | Africa        |
|     1.5  | AL     | albania                                      | shinkin     | Europe        |
|     1.5  | BS     | bahamas                                      | shinkin     | North America |
|     1.5  | BH     | bahrain                                      | shinkin     | Asia          |
|     1.5  | AF     | afghanistan                                  | shinkin     | Asia          |
|     1.5  | BV     | bouvet island                                | shinkin     |               |
|     1.5  | BW     | botswana                                     | shinkin     | Africa        |
|     1.5  | BA     | bosnia and herzegovina                       | shinkin     | Europe        |
|     1.5  | BQ     | bonaire, sint eustatius and saba             | shinkin     |               |
|     1.5  | BO     | bolivia                                      | shinkin     | South America |
|     1.5  | BT     | bhutan                                       | shinkin     | Asia          |
|     1.5  | BM     | bermuda                                      | shinkin     |               |
|     1.5  | BJ     | benin                                        | shinkin     | Africa        |
|     1.5  | BZ     | belize                                       | shinkin     | North America |
|     1.5  | BY     | belarus                                      | shinkin     | Europe        |
|     1.5  | BB     | barbados                                     | shinkin     | North America |
|     1.5  | BD     | bangladesh                                   | shinkin     | Asia          |
|     1.5  | BD     | aland islands                                | shinkin     |               |
|     1.5  | MO     | macao                                        | shinkin     |               |
|     1.5  | JO     | jordan                                       | shinkin     | Asia          |
|     1.5  | JE     | jersey                                       | shinkin     |               |
|     1.5  | JP     | japan                                        | shinkin     | Asia          |
|     1.5  | JM     | jamaica                                      | shinkin     | North America |
|     1.5  | IT     | italy                                        | shinkin     | Europe        |
|     1.5  | IL     | israel                                       | shinkin     | Asia          |
|     1.5  | IM     | isle of man                                  | shinkin     |               |
|     1.5  | IQ     | iraq                                         | shinkin     | Asia          |
|     1.5  | IQ     | iran                                         | shinkin     |               |
|     1.5  | ID     | indonesia                                    | shinkin     | Asia          |
|     1.5  | IN     | india                                        | shinkin     | Asia          |
|     1.5  | IS     | iceland                                      | shinkin     | Europe        |
|     1.5  | HU     | hungary                                      | shinkin     | Europe        |
|     1.5  | KZ     | kazakhstan                                   | shinkin     | Asia          |
|     1.5  | KE     | kenya                                        | shinkin     | Africa        |
|     1.5  | LU     | luxembourg                                   | shinkin     | Europe        |
|     1.5  | LT     | lithuania                                    | shinkin     | Europe        |
|     1.5  | LI     | liechtenstein                                | shinkin     | Europe        |
|     1.5  | LY     | libya                                        | shinkin     | Africa        |
|     1.5  | LR     | liberia                                      | shinkin     | Africa        |
|     1.5  | LS     | lesotho                                      | shinkin     | Africa        |
|     1.5  | LB     | lebanon                                      | shinkin     | Asia          |
|     1.5  | LV     | latvia                                       | shinkin     | Europe        |
|     1.5  | LV     | laos                                         | shinkin     |               |
|     1.5  | KG     | kyrgyzstan                                   | shinkin     | Asia          |
|     1.5  | KW     | kuwait                                       | shinkin     | Asia          |
|     1.5  | KW     | kosovo                                       | shinkin     |               |
|     1.5  | KI     | kiribati                                     | shinkin     | Oceania       |
|     1.5  | HK     | hong kong                                    | shinkin     |               |
|     1.5  | HN     | honduras                                     | shinkin     | North America |
|     1.5  | HM     | heard island and mcdonald islands            | shinkin     |               |
|     1.5  | GA     | gabon                                        | shinkin     | Africa        |
|     1.5  | TF     | french southern territories                  | shinkin     |               |
|     1.5  | PF     | french polynesia                             | shinkin     |               |
|     1.5  | GF     | french guiana                                | shinkin     |               |
|     1.5  | FR     | france                                       | shinkin     | Europe        |
|     1.5  | FJ     | fiji                                         | shinkin     | Oceania       |
|     1.5  | FO     | faroe islands                                | shinkin     |               |
|     1.5  | FK     | falkland islands (malvinas)                  | shinkin     |               |
|     1.5  | ET     | ethiopia                                     | shinkin     | Africa        |
|     1.5  | EE     | estonia                                      | shinkin     | Europe        |
|     1.5  | ER     | eritrea                                      | shinkin     | Africa        |
|     1.5  | GQ     | equatorial guinea                            | shinkin     | Africa        |
|     1.5  | SV     | el salvador                                  | shinkin     | North America |
|     1.5  | GM     | gambia                                       | shinkin     | Africa        |
|     1.5  | GE     | georgia                                      | shinkin     | Asia          |
|     1.5  | HT     | haiti                                        | shinkin     |               |
|     1.5  | GW     | guinea-bissau                                | shinkin     | Africa        |
|     1.5  | GN     | guinea                                       | shinkin     | Africa        |
|     1.5  | GG     | guernsey                                     | shinkin     |               |
|     1.5  | GT     | guatemala                                    | shinkin     | North America |
|     1.5  | GU     | guam                                         | shinkin     |               |
|     1.5  | GU     | guadaloupe                                   | shinkin     |               |
|     1.5  | GD     | grenada                                      | shinkin     | North America |
|     1.5  | GL     | greenland                                    | shinkin     |               |
|     1.5  | GR     | greece                                       | shinkin     | Europe        |
|     1.5  | GI     | gibraltar                                    | shinkin     |               |
|     1.5  | GH     | ghana                                        | shinkin     | Africa        |
|     1.5  | DE     | germany                                      | shinkin     | Europe        |
|     1.5  | EG     | egypt                                        | shinkin     | Africa        |
|     1.5  | BR     | <b>all other country</b>                     | ouoio       |               |
|     1.5  | ES     | spain                                        | uskipme     | Europe        |
|     1.5  | CH     | switzerland                                  | uskipme     | Europe        |
|     1.5  | SG     | singapore                                    | uskipme     | Asia          |
|     1.5  | AT     | austria                                      | uskipme     | Europe        |
|     1.5  | ID     | indonesia                                    | uskipme     | Asia          |
|     1.47 | GU     | guam                                         | shorte      |               |
|     1.46 | BS     | st. vincent & grenadines                     | shorte      |               |
|     1.44 | LU     | macau sar china                              | shorte      |               |
|     1.44 | KN     | saint kitts and nevis                        | shorte      | North America |
|     1.43 | MH     | marshall islands                             | shorte      | Oceania       |
|     1.42 | BB     | barbados                                     | shorte      | North America |
|     1.42 | LT     | lithuania                                    | shorte      | Europe        |
|     1.42 | LY     | macedonia                                    | shorte      |               |
|     1.4  | MT     | malta                                        | shorte      | Europe        |
|     1.39 | CO     | brasil                                       | shorte      |               |
|     1.39 | CY     | cte divoire                                  | shorte      |               |
|     1.38 | PH     | philippines                                  | shorte      | Asia          |
|     1.37 | BS     | bahamas                                      | shorte      | North America |
|     1.37 | TD     | chad                                         | shorte      | Africa        |
|     1.34 | KY     | cayman islands                               | shorte      |               |
|     1.33 | KI     | Kiribati                                     | bcvc        | Oceania       |
|     1.33 | CN     | china                                        | shorte      | Asia          |
|     1.31 | NC     | new caledonia                                | shorte      |               |
|     1.31 | DM     | dominica                                     | shorte      | North America |
|     1.3  | IL     | Israel                                       | bcvc        | Asia          |
|     1.3  | ER     | eritrea                                      | shorte      | Africa        |
|     1.3  | TZ     | tanzania                                     | shorte      | Africa        |
|     1.3  | TZ     | land islands                                 | shorte      |               |
|     1.3  | IN     | brunei                                       | shorte      |               |
|     1.3  | JO     | jordan                                       | shorte      | Asia          |
|     1.3  | TR     | turkey                                       | uskipme     | Asia          |
|     1.3  | BR     | brazil                                       | uskipme     | South America |
|     1.29 | BY     | belarus                                      | shorte      | Europe        |
|     1.29 | GL     | greenland                                    | shorte      |               |
|     1.27 | BD     | bangladesh                                   | shorte      | Asia          |
|     1.26 | TC     | turks and caicos islands                     | shorte      |               |
|     1.26 | LC     | saint lucia                                  | shorte      | North America |
|     1.26 | FJ     | fiji                                         | shorte      | Oceania       |
|     1.26 | ME     | montenegro                                   | shorte      | Europe        |
|     1.25 | ZW     | u.s. virgin islands                          | shorte      |               |
|     1.25 | AG     | antigua and barbuda                          | shorte      | North America |
|     1.23 | HK     | Hong Kong                                    | bcvc        |               |
|     1.2  | TH     | Thailand                                     | bcvc        | Asia          |
|     1.2  | BR     | Brazil                                       | bcvc        | South America |
|     1.2  | EC     | Ecuador                                      | bcvc        | South America |
|     1.2  | EE     | vatican city                                 | shorte      |               |
|     1.2  | IM     | isle of man                                  | shorte      |               |
|     1.2  | LR     | liberia                                      | shorte      | Africa        |
|     1.2  | AM     | armenia                                      | uskipme     | Asia          |
|     1.2  | LT     | lithuania                                    | uskipme     | Europe        |
|     1.2  | GE     | georgia                                      | uskipme     | Asia          |
|     1.2  | MX     | mexico                                       | uskipme     | North America |
|     1.2  | AO     | angola                                       | uskipme     | Africa        |
|     1.18 | AL     | albania                                      | shorte      | Europe        |
|     1.17 | CY     | Cyprus                                       | bcvc        | Asia          |
|     1.17 | AS     | american samoa                               | shorte      |               |
|     1.17 | ID     | indonesia                                    | shorte      | Asia          |
|     1.16 | PR     | puerto rico                                  | shorte      |               |
|     1.16 | PW     | palau                                        | shorte      | Oceania       |
|     1.16 | JE     | jersey                                       | shorte      |               |
|     1.16 | LK     | sri lanka                                    | shorte      | Asia          |
|     1.16 | MD     | moldova                                      | shorte      | Europe        |
|     1.16 | MD     | runion                                       | shorte      |               |
|     1.16 | KE     | kenya                                        | shorte      | Africa        |
|     1.14 | HT     | Haiti                                        | bcvc        |               |
|     1.14 | GG     | guernsey                                     | shorte      |               |
|     1.14 | MC     | monaco                                       | shorte      | Europe        |
|     1.14 | SD     | sudan                                        | shorte      | Africa        |
|     1.14 | SD     | micronesia                                   | shorte      |               |
|     1.14 | VU     | vanuatu                                      | shorte      | Oceania       |
|     1.14 | GI     | gibraltar                                    | shorte      |               |
|     1.14 | TT     | trinidad and tobago                          | shorte      | North America |
|     1.14 | DO     | dominican republic                           | shorte      | North America |
|     1.13 | KZ     | kazakhstan                                   | shorte      | Asia          |
|     1.13 | HR     | croatia                                      | shorte      | Europe        |
|     1.12 | EG     | egypt                                        | shorte      | Africa        |
|     1.12 | DZ     | algeria                                      | shorte      | Africa        |
|     1.11 | SN     | senegal                                      | shorte      | Africa        |
|     1.1  | MY     | Malaysia                                     | bcvc        | Asia          |
|     1.1  | IN     | India                                        | bcvc        | Asia          |
|     1.1  | AU     | mxico                                        | shorte      |               |
|     1.1  | HR     | croatia                                      | uskipme     | Europe        |
|     1.1  | CO     | colombia                                     | uskipme     | South America |
|     1.1  | BG     | bulgaria                                     | uskipme     | Europe        |
|     1.1  | CL     | chile                                        | uskipme     | South America |
|     1.1  | AZ     | azerbaijan                                   | uskipme     | Asia          |
|     1.1  | AR     | argentina                                    | uskipme     | South America |
|     1.1  | GN     | guinea                                       | uskipme     | Africa        |
|     1.1  | VE     | venezuela, bolivarian republic of            | uskipme     | South America |
|     1.09 | TN     | tunisia                                      | shorte      | Africa        |
|     1.08 | KH     | cambodia                                     | uskipme     | Asia          |
|     1.08 | PR     | puerto rico                                  | uskipme     |               |
|     1.08 | BI     | burundi                                      | uskipme     | Africa        |
|     1.07 | PF     | french polynesia                             | shorte      |               |
|     1.05 | MQ     | martinique                                   | shorte      |               |
|     1.05 | NR     | nauru                                        | uskipme     | Oceania       |
|     1.05 | TN     | tunisia                                      | uskipme     | Africa        |
|     1.05 | AF     | afghanistan                                  | uskipme     | Asia          |
|     1.05 | TZ     | tanzania, united republic of                 | uskipme     | Africa        |
|     1.04 | LS     | lesotho                                      | shorte      | Africa        |
|     1.04 | DJ     | djibouti                                     | shorte      | Africa        |
|     1.04 | CM     | cameroon                                     | shorte      | Africa        |
|     1.04 | AD     | andorra                                      | shorte      | Europe        |
|     1.04 | AD     | iran                                         | shorte      |               |
|     1.04 | SM     | san marino                                   | shorte      | Europe        |
|     1.04 | LI     | liechtenstein                                | shorte      | Europe        |
|     1.04 | NA     | namibia                                      | shorte      | Africa        |
|     1.04 | SC     | seychelles                                   | shorte      | Africa        |
|     1.04 | ZW     | zimbabwe                                     | shorte      | Africa        |
|     1.04 | UG     | uganda                                       | shorte      | Africa        |
|     1.04 | GH     | ghana                                        | shorte      | Africa        |
|     1.04 | GH     | so tom and prncipe                           | shorte      |               |
|     1.04 | AZ     | azerbaijan                                   | shorte      | Asia          |
|     1.04 | KH     | cambodia                                     | shorte      | Asia          |
|     1.04 | SO     | somalia                                      | shorte      | Africa        |
|     1.04 | AM     | armenia                                      | shorte      | Asia          |
|     1.02 | PA     | panama                                       | shorte      | North America |
|     1.02 | NC     | cape verde                                   | shorte      |               |
|     1.01 | JP     | south korea                                  | shorte      |               |
|     1.01 | VN     | vietnam                                      | shorte      | Asia          |
|     1.01 | EC     | ecuador                                      | shorte      | South America |
|     1    | UY     | Uruguay                                      | bcvc        | South America |
|     1    | MM     | Myanmar                                      | bcvc        | Asia          |
|     1    | MX     | Mexico                                       | bcvc        | North America |
|     1    | FJ     | Fiji                                         | bcvc        | Oceania       |
|     1    | FK     | Falkland Islands (Malvinas)                  | bcvc        |               |
|     1    | SB     | Solomon Islands                              | bcvc        | Oceania       |
|     1    | MW     | Malawi                                       | bcvc        | Africa        |
|     1    | MW     | Virgin Islands (U.S.)                        | bcvc        |               |
|     1    | SA     | Saudi Arabia                                 | bcvc        | Asia          |
|     1    | CL     | Chile                                        | bcvc        | South America |
|     1    | RO     | Romania                                      | bcvc        | Europe        |
|     1    | NU     | Niue                                         | bcvc        |               |
|     1    | KM     | Comoros                                      | bcvc        | Africa        |
|     1    | PE     | Peru                                         | bcvc        | South America |
|     1    | NC     | u.s. outlying islands                        | shorte      |               |
|     1    | NU     | niue                                         | shorte      |               |
|     1    | TZ     | macedonia, the former yugoslav republic of   | uskipme     |               |
|     1    | RS     | serbia                                       | uskipme     | Europe        |
|     1    | ME     | montenegro                                   | uskipme     | Europe        |
|     1    | SC     | seychelles                                   | uskipme     | Africa        |
|     1    | SL     | sierra leone                                 | uskipme     | Africa        |
|     1    | KP     | korea, democratic people's republic of       | uskipme     | Asia          |
|     1    | KI     | kiribati                                     | uskipme     | Oceania       |
|     1    | SV     | el salvador                                  | uskipme     | North America |
|     1    | CZ     | czech republic                               | uskipme     | Europe        |
|     1    | IO     | british indian ocean territory               | uskipme     |               |
|     1    | PW     | palau                                        | uskipme     | Oceania       |
|     1    | UG     | uganda                                       | uskipme     | Africa        |
|     1    | CU     | cuba                                         | uskipme     | North America |
|     1    | TV     | tuvalu                                       | uskipme     | Oceania       |
|     1    | TG     | togo                                         | uskipme     | Africa        |
|     1    | TG     | cote d'ivoire                                | uskipme     |               |
|     0.99 | GP     | guadeloupe                                   | shorte      |               |
|     0.98 | AO     | Angola                                       | bcvc        | Africa        |
|     0.98 | GY     | guyana                                       | shorte      | South America |
|     0.98 | TO     | tonga                                        | shorte      | Oceania       |
|     0.98 | TO     | netherlands antilles                         | shorte      |               |
|     0.98 | TO     | saint martin                                 | shorte      |               |
|     0.98 | WS     | samoa                                        | shorte      | Oceania       |
|     0.98 | CU     | cuba                                         | shorte      | North America |
|     0.96 | UG     | Uganda                                       | bcvc        | Africa        |
|     0.96 | GF     | french guiana                                | shorte      |               |
|     0.95 | KE     | Kenya                                        | bcvc        | Africa        |
|     0.95 | PT     | portugal                                     | uskipme     | Europe        |
|     0.95 | ZW     | zimbabwe                                     | uskipme     | Africa        |
|     0.95 | KZ     | kazakhstan                                   | uskipme     | Asia          |
|     0.95 | TM     | turkmenistan                                 | uskipme     | Asia          |
|     0.95 | BJ     | benin                                        | uskipme     | Africa        |
|     0.95 | TO     | tonga                                        | uskipme     | Oceania       |
|     0.95 | LS     | lesotho                                      | uskipme     | Africa        |
|     0.95 | PH     | philippines                                  | uskipme     | Asia          |
|     0.95 | BN     | brunei darussalam                            | uskipme     | Asia          |
|     0.95 | GF     | french guiana                                | uskipme     |               |
|     0.95 | SZ     | swaziland                                    | uskipme     | Africa        |
|     0.95 | CR     | costa rica                                   | uskipme     | North America |
|     0.95 | IT     | italy                                        | uskipme     | Europe        |
|     0.95 | ET     | ethiopia                                     | uskipme     | Africa        |
|     0.95 | CD     | congo, the democratic republic of the        | uskipme     | Africa        |
|     0.95 | LR     | liberia                                      | uskipme     | Africa        |
|     0.95 | JO     | jordan                                       | uskipme     | Asia          |
|     0.94 | KE     | Netherlands Antilles                         | bcvc        |               |
|     0.94 | GQ     | equatorial guinea                            | shorte      | Africa        |
|     0.93 | GL     | Greenland                                    | bcvc        |               |
|     0.93 | KN     | Saint Kitts And Nevis                        | bcvc        | North America |
|     0.92 | GH     | Ghana                                        | bcvc        | Africa        |
|     0.92 | GH     | East Timor                                   | bcvc        |               |
|     0.92 | SC     | Seychelles                                   | bcvc        | Africa        |
|     0.92 | SC     | St. Pierre And Miquelon                      | bcvc        |               |
|     0.91 | BW     | Botswana                                     | bcvc        | Africa        |
|     0.91 | GU     | Guam                                         | bcvc        |               |
|     0.91 | HN     | Honduras                                     | bcvc        | North America |
|     0.9  | MG     | Madagascar                                   | bcvc        | Africa        |
|     0.9  | KG     | kyrgyzstan                                   | shorte      | Asia          |
|     0.89 | MG     | Croatia (Local Name: Hrvatska)               | bcvc        |               |
|     0.89 | BM     | Bermuda                                      | bcvc        |               |
|     0.89 | PF     | French Polynesia                             | bcvc        |               |
|     0.88 | CR     | Costa Rica                                   | bcvc        | North America |
|     0.88 | CU     | Cuba                                         | bcvc        | North America |
|     0.88 | VC     | Saint Vincent And The Grenadines             | bcvc        | North America |
|     0.88 | KY     | Cayman Islands                               | bcvc        |               |
|     0.88 | GD     | grenada                                      | shorte      | North America |
|     0.88 | YT     | mayotte                                      | shorte      |               |
|     0.88 | BT     | bhutan                                       | shorte      | Asia          |
|     0.88 | AW     | aruba                                        | shorte      |               |
|     0.88 | BZ     | belize                                       | shorte      | North America |
|     0.87 | BW     | botswana                                     | shorte      | Africa        |
|     0.86 | AL     | Albania                                      | bcvc        | Europe        |
|     0.86 | MZ     | Mozambique                                   | bcvc        | Africa        |
|     0.86 | SY     | Syrian Arab Republic                         | bcvc        | Asia          |
|     0.86 | NC     | New Caledonia                                | bcvc        |               |
|     0.86 | MV     | maldives                                     | shorte      | Asia          |
|     0.86 | VG     | british virgin islands                       | shorte      |               |
|     0.86 | CR     | costa rica                                   | shorte      | North America |
|     0.86 | SR     | suriname                                     | shorte      | South America |
|     0.86 | BM     | bermuda                                      | shorte      |               |
|     0.86 | NE     | niger                                        | shorte      | Africa        |
|     0.86 | WF     | wallis and futuna                            | shorte      |               |
|     0.86 | CN     | china                                        | uskipme     | Asia          |
|     0.85 | NC     | Bosnia And Herzegowina                       | bcvc        |               |
|     0.85 | EG     | Egypt                                        | bcvc        | Africa        |
|     0.85 | PK     | pakistan                                     | shorte      | Asia          |
|     0.85 | SL     | sierra leone                                 | shorte      | Africa        |
|     0.85 | MT     | malta                                        | uskipme     | Europe        |
|     0.85 | CG     | congo                                        | uskipme     | Africa        |
|     0.85 | NP     | nepal                                        | uskipme     | Asia          |
|     0.85 | NU     | niue                                         | uskipme     |               |
|     0.85 | MP     | northern mariana islands                     | uskipme     |               |
|     0.85 | SO     | somalia                                      | uskipme     | Africa        |
|     0.85 | RW     | rwanda                                       | uskipme     | Africa        |
|     0.85 | KY     | cayman islands                               | uskipme     |               |
|     0.85 | HK     | hong kong                                    | uskipme     |               |
|     0.85 | GU     | guam                                         | uskipme     |               |
|     0.85 | BO     | bolivia, plurinational state of              | uskipme     | South America |
|     0.85 | HN     | honduras                                     | uskipme     | North America |
|     0.85 | TD     | chad                                         | uskipme     | Africa        |
|     0.85 | SB     | solomon islands                              | uskipme     | Oceania       |
|     0.84 | EG     | Virgin Islands (British)                     | bcvc        |               |
|     0.84 | SM     | San Marino                                   | bcvc        | Europe        |
|     0.83 | FO     | Faroe Islands                                | bcvc        |               |
|     0.83 | CD     | Congo, The Democratic Republic Of The        | bcvc        | Africa        |
|     0.83 | AM     | Armenia                                      | bcvc        | Asia          |
|     0.83 | UA     | ukraine                                      | shorte      | Europe        |
|     0.83 | ET     | ethiopia                                     | shorte      | Africa        |
|     0.82 | BN     | Brunei Darussalam                            | bcvc        | Asia          |
|     0.82 | MU     | Mauritius                                    | bcvc        | Africa        |
|     0.82 | GR     | greece                                       | shorte      | Europe        |
|     0.82 | ZM     | zambia                                       | shorte      | Africa        |
|     0.81 | PR     | Puerto Rico                                  | bcvc        |               |
|     0.81 | MC     | Monaco                                       | bcvc        | Europe        |
|     0.81 | ET     | Ethiopia                                     | bcvc        | Africa        |
|     0.81 | BB     | Barbados                                     | bcvc        | North America |
|     0.81 | CL     | chile                                        | shorte      | South America |
|     0.8  | CO     | Colombia                                     | bcvc        | South America |
|     0.8  | AR     | Argentina                                    | bcvc        | South America |
|     0.8  | ID     | Indonesia                                    | bcvc        | Asia          |
|     0.8  | LV     | Latvia                                       | bcvc        | Europe        |
|     0.8  | GP     | Guadeloupe                                   | bcvc        |               |
|     0.8  | BO     | Bolivia                                      | bcvc        | South America |
|     0.8  | NF     | Norfolk Island                               | bcvc        |               |
|     0.8  | RO     | romania                                      | shorte      | Europe        |
|     0.8  | TW     | taiwan                                       | shorte      |               |
|     0.8  | JM     | jamaica                                      | uskipme     | North America |
|     0.79 | CM     | Cameroon                                     | bcvc        | Africa        |
|     0.79 | TZ     | Tanzania, United Republic Of                 | bcvc        | Africa        |
|     0.79 | MP     | Northern Mariana Islands                     | bcvc        |               |
|     0.79 | JM     | Jamaica                                      | bcvc        | North America |
|     0.79 | KM     | comoros                                      | shorte      | Africa        |
|     0.79 | BA     | bosnia and herzegovina                       | shorte      | Europe        |
|     0.78 | PA     | Panama                                       | bcvc        | North America |
|     0.78 | MA     | morocco                                      | shorte      | Africa        |
|     0.78 | BY     | belarus                                      | uskipme     | Europe        |
|     0.77 | GT     | Guatemala                                    | bcvc        | North America |
|     0.77 | NI     | Nicaragua                                    | bcvc        | North America |
|     0.77 | DO     | Dominican Republic                           | bcvc        | North America |
|     0.77 | BJ     | benin                                        | shorte      | Africa        |
|     0.77 | GQ     | laos                                         | shorte      |               |
|     0.76 | DO     | Cote D'Ivoire                                | bcvc        |               |
|     0.76 | ZW     | Zimbabwe                                     | bcvc        | Africa        |
|     0.76 | NG     | Nigeria                                      | bcvc        | Africa        |
|     0.76 | CK     | cook islands                                 | shorte      |               |
|     0.75 | TT     | Trinidad And Tobago                          | bcvc        | North America |
|     0.75 | KG     | Kyrgyzstan                                   | bcvc        | Asia          |
|     0.75 | BG     | Bulgaria                                     | bcvc        | Europe        |
|     0.75 | GF     | French Guiana                                | bcvc        |               |
|     0.75 | TL     | falkland islands                             | shorte      |               |
|     0.75 | AI     | anguilla                                     | shorte      |               |
|     0.75 | SZ     | swaziland                                    | shorte      | Africa        |
|     0.75 | RW     | rwanda                                       | shorte      | Africa        |
|     0.75 | MG     | madagascar                                   | shorte      | Africa        |
|     0.75 | WF     | wallis and futuna                            | uskipme     |               |
|     0.75 | BA     | bosnia and herzegovina                       | uskipme     | Europe        |
|     0.75 | BD     | bangladesh                                   | uskipme     | Asia          |
|     0.75 | PY     | paraguay                                     | uskipme     | South America |
|     0.75 | UY     | uruguay                                      | uskipme     | South America |
|     0.75 | FJ     | fiji                                         | uskipme     | Oceania       |
|     0.75 | FJ     | reunion                                      | uskipme     |               |
|     0.75 | NL     | netherlands                                  | uskipme     | Europe        |
|     0.75 | GP     | guadeloupe                                   | uskipme     |               |
|     0.75 | MN     | mongolia                                     | uskipme     | Asia          |
|     0.75 | GT     | guatemala                                    | uskipme     | North America |
|     0.75 | MO     | macao                                        | uskipme     |               |
|     0.75 | NF     | norfolk island                               | uskipme     |               |
|     0.75 | TH     | thailand                                     | uskipme     | Asia          |
|     0.74 | MR     | Mauritania                                   | bcvc        | Africa        |
|     0.74 | MQ     | Martinique                                   | bcvc        |               |
|     0.74 | BY     | Belarus                                      | bcvc        | Europe        |
|     0.74 | ML     | mali                                         | shorte      | Africa        |
|     0.73 | LA     | Lao People's Democratic Republic             | bcvc        | Asia          |
|     0.73 | SD     | Sudan                                        | bcvc        | Africa        |
|     0.73 | HT     | haiti                                        | shorte      |               |
|     0.72 | EE     | Estonia                                      | bcvc        | Europe        |
|     0.72 | TL     | timor-leste                                  | shorte      | Asia          |
|     0.72 | UZ     | uzbekistan                                   | shorte      | Asia          |
|     0.72 | MS     | montserrat                                   | shorte      |               |
|     0.72 | GN     | guinea                                       | shorte      | Africa        |
|     0.72 | TM     | turkmenistan                                 | shorte      | Asia          |
|     0.72 | ZM     | zambia                                       | uskipme     | Africa        |
|     0.72 | IR     | iran, islamic republic of                    | uskipme     | Asia          |
|     0.72 | BB     | barbados                                     | uskipme     | North America |
|     0.72 | LA     | lao people's democratic republic             | uskipme     | Asia          |
|     0.71 | ZM     | Zambia                                       | bcvc        | Africa        |
|     0.71 | BS     | Bahamas                                      | bcvc        | North America |
|     0.71 | BS     | Libyan Arab Jamahiriya                       | bcvc        |               |
|     0.7  | UA     | Ukraine                                      | bcvc        | Europe        |
|     0.7  | GI     | Gibraltar                                    | bcvc        |               |
|     0.7  | JP     | Japan                                        | bcvc        | Asia          |
|     0.7  | EG     | saint barthlemy                              | shorte      |               |
|     0.7  | RS     | serbia                                       | shorte      | Europe        |
|     0.7  | TG     | togo                                         | shorte      | Africa        |
|     0.7  | MU     | mauritius                                    | shorte      | Africa        |
|     0.7  | LY     | libya                                        | shorte      | Africa        |
|     0.7  | TT     | trinidad and tobago                          | uskipme     | North America |
|     0.7  | CM     | cameroon                                     | uskipme     | Africa        |
|     0.7  | FR     | france                                       | uskipme     | Europe        |
|     0.7  | GW     | guinea-bissau                                | uskipme     | Africa        |
|     0.7  | MW     | malawi                                       | uskipme     | Africa        |
|     0.7  | GY     | guyana                                       | uskipme     | South America |
|     0.7  | FO     | faroe islands                                | uskipme     |               |
|     0.7  | HT     | haiti                                        | uskipme     |               |
|     0.7  | MG     | madagascar                                   | uskipme     | Africa        |
|     0.7  | LY     | libya                                        | uskipme     | Africa        |
|     0.7  | GQ     | equatorial guinea                            | uskipme     | Africa        |
|     0.7  | DO     | dominican republic                           | uskipme     | North America |
|     0.7  | DO     | cape verde                                   | uskipme     |               |
|     0.7  | DJ     | djibouti                                     | uskipme     | Africa        |
|     0.7  | DJ     | aland islands                                | uskipme     |               |
|     0.7  | AD     | andorra                                      | uskipme     | Europe        |
|     0.7  | VI     | virgin islands, u.s.                         | uskipme     |               |
|     0.7  | TL     | timor-leste                                  | uskipme     | Asia          |
|     0.7  | BF     | burkina faso                                 | uskipme     | Africa        |
|     0.7  | SY     | syrian arab republic                         | uskipme     | Asia          |
|     0.7  | UA     | ukraine                                      | uskipme     | Europe        |
|     0.7  | VU     | vanuatu                                      | uskipme     | Oceania       |
|     0.7  | BW     | botswana                                     | uskipme     | Africa        |
|     0.7  | BT     | bhutan                                       | uskipme     | Asia          |
|     0.7  | BZ     | belize                                       | uskipme     | North America |
|     0.7  | BE     | belgium                                      | uskipme     | Europe        |
|     0.7  | GA     | gabon                                        | uskipme     | Africa        |
|     0.7  | BS     | bahamas                                      | uskipme     | North America |
|     0.7  | GM     | gambia                                       | uskipme     | Africa        |
|     0.7  | GI     | gibraltar                                    | uskipme     |               |
|     0.7  | GL     | greenland                                    | uskipme     |               |
|     0.7  | GD     | grenada                                      | uskipme     | North America |
|     0.7  | TJ     | tajikistan                                   | uskipme     | Asia          |
|     0.7  | NA     | namibia                                      | uskipme     | Africa        |
|     0.7  | MU     | mauritius                                    | uskipme     | Africa        |
|     0.7  | SM     | san marino                                   | uskipme     | Europe        |
|     0.7  | NI     | nicaragua                                    | uskipme     | North America |
|     0.7  | SN     | senegal                                      | uskipme     | Africa        |
|     0.7  | MS     | montserrat                                   | uskipme     |               |
|     0.7  | FM     | micronesia, federated states of              | uskipme     | Oceania       |
|     0.7  | YT     | mayotte                                      | uskipme     |               |
|     0.7  | MR     | mauritania                                   | uskipme     | Africa        |
|     0.7  | VC     | saint vincent and the grenadines             | uskipme     | North America |
|     0.7  | MF     | saint martin (french part)                   | uskipme     |               |
|     0.7  | MZ     | mozambique                                   | uskipme     | Africa        |
|     0.7  | SR     | suriname                                     | uskipme     | South America |
|     0.7  | ST     | sao tome and principe                        | uskipme     | Africa        |
|     0.7  | RO     | romania                                      | uskipme     | Europe        |
|     0.7  | MC     | monaco                                       | uskipme     | Europe        |
|     0.7  | ML     | mali                                         | uskipme     | Africa        |
|     0.69 | AG     | Antigua And Barbuda                          | bcvc        | North America |
|     0.69 | MA     | Morocco                                      | bcvc        | Africa        |
|     0.69 | MN     | Mongolia                                     | bcvc        | Asia          |
|     0.69 | TJ     | tajikistan                                   | shorte      | Asia          |
|     0.68 | LK     | Sri Lanka                                    | bcvc        | Asia          |
|     0.68 | PH     | Philippines                                  | bcvc        | Asia          |
|     0.68 | SN     | Senegal                                      | bcvc        | Africa        |
|     0.68 | SN     | Reunion                                      | bcvc        |               |
|     0.68 | TN     | Tunisia                                      | bcvc        | Africa        |
|     0.68 | BF     | burkina faso                                 | shorte      | Africa        |
|     0.67 | TW     | Taiwan                                       | bcvc        |               |
|     0.67 | BZ     | Belize                                       | bcvc        | North America |
|     0.67 | KH     | Cambodia                                     | bcvc        | Asia          |
|     0.67 | MD     | Moldova, Republic Of                         | bcvc        | Europe        |
|     0.66 | AW     | Aruba                                        | bcvc        |               |
|     0.66 | DZ     | Algeria                                      | bcvc        | Africa        |
|     0.66 | KP     | Korea, Democratic People's Republic Of       | bcvc        | Asia          |
|     0.66 | MW     | malawi                                       | shorte      | Africa        |
|     0.66 | MZ     | mozambique                                   | shorte      | Africa        |
|     0.66 | PE     | peru                                         | shorte      | South America |
|     0.66 | EC     | congo - brazzaville                          | shorte      |               |
|     0.66 | GM     | gambia                                       | shorte      | Africa        |
|     0.66 | MN     | mongolia                                     | shorte      | Asia          |
|     0.65 | PK     | Pakistan                                     | bcvc        | Asia          |
|     0.65 | BD     | Bangladesh                                   | bcvc        | Asia          |
|     0.65 | GY     | Guyana                                       | bcvc        | South America |
|     0.65 | LI     | Liechtenstein                                | bcvc        | Europe        |
|     0.65 | HU     | hungary                                      | shorte      | Europe        |
|     0.65 | PE     | peru                                         | uskipme     | South America |
|     0.65 | LI     | liechtenstein                                | uskipme     | Europe        |
|     0.65 | MD     | moldova, republic of                         | uskipme     | Europe        |
|     0.65 | AW     | aruba                                        | uskipme     |               |
|     0.65 | UZ     | uzbekistan                                   | uskipme     | Asia          |
|     0.65 | LC     | saint lucia                                  | uskipme     | North America |
|     0.65 | KN     | saint kitts and nevis                        | uskipme     | North America |
|     0.65 | NC     | new caledonia                                | uskipme     |               |
|     0.65 | AG     | antigua and barbuda                          | uskipme     | North America |
|     0.65 | KG     | kyrgyzstan                                   | uskipme     | Asia          |
|     0.65 | MH     | marshall islands                             | uskipme     | Oceania       |
|     0.65 | PG     | papua new guinea                             | uskipme     | Oceania       |
|     0.65 | LK     | sri lanka                                    | uskipme     | Asia          |
|     0.65 | MQ     | martinique                                   | uskipme     |               |
|     0.65 | MY     | malaysia                                     | uskipme     | Asia          |
|     0.65 | KR     | korea, republic of                           | uskipme     | Asia          |
|     0.65 | KM     | comoros                                      | uskipme     | Africa        |
|     0.64 | GE     | Georgia                                      | bcvc        | Asia          |
|     0.64 | AR     | argentina                                    | shorte      | South America |
|     0.64 | TV     | tuvalu                                       | shorte      | Oceania       |
|     0.64 | AO     | angola                                       | shorte      | Africa        |
|     0.64 | GT     | guatemala                                    | shorte      | North America |
|     0.64 | KI     | north korea                                  | shorte      |               |
|     0.63 | GM     | Gambia                                       | bcvc        | Africa        |
|     0.63 | BJ     | Benin                                        | bcvc        | Africa        |
|     0.63 | DJ     | Djibouti                                     | bcvc        | Africa        |
|     0.63 | MT     | Malta                                        | bcvc        | Europe        |
|     0.62 | MV     | Maldives                                     | bcvc        | Asia          |
|     0.62 | MR     | mauritania                                   | shorte      | Africa        |
|     0.62 | GA     | gabon                                        | shorte      | Africa        |
|     0.62 | BI     | burundi                                      | shorte      | Africa        |
|     0.62 | PF     | french polynesia                             | uskipme     |               |
|     0.61 | MV     | Macedonia, Former Yugoslav Republic Of       | bcvc        |               |
|     0.61 | BF     | Burkina Faso                                 | bcvc        | Africa        |
|     0.61 | BF     | Iran (Islamic Republic Of)                   | bcvc        |               |
|     0.61 | NP     | Nepal                                        | bcvc        | Asia          |
|     0.61 | NP     | nepal                                        | shorte      | Asia          |
|     0.61 | JM     | syria                                        | shorte      |               |
|     0.6  | YE     | Yemen                                        | bcvc        | Asia          |
|     0.6  | DM     | Dominica                                     | bcvc        | North America |
|     0.6  | PY     | paraguay                                     | shorte      | South America |
|     0.6  | PE     | myanmar (burma)                              | shorte      |               |
|     0.6  | JM     | jamaica                                      | shorte      | North America |
|     0.6  | VE     | venezuela                                    | shorte      | South America |
|     0.59 | SR     | Suriname                                     | bcvc        | South America |
|     0.59 | CO     | colombia                                     | shorte      | South America |
|     0.59 | HN     | honduras                                     | shorte      | North America |
|     0.59 | KI     | kiribati                                     | shorte      | Oceania       |
|     0.58 | VN     | Viet Nam                                     | bcvc        | Asia          |
|     0.58 | UZ     | Uzbekistan                                   | bcvc        | Asia          |
|     0.57 | GA     | Gabon                                        | bcvc        | Africa        |
|     0.57 | KZ     | Kazakhstan                                   | bcvc        | Asia          |
|     0.57 | ML     | Mali                                         | bcvc        | Africa        |
|     0.57 | CF     | central african republic                     | shorte      | Africa        |
|     0.57 | VE     | palestinian territories                      | shorte      |               |
|     0.56 | AD     | Andorra                                      | bcvc        | Europe        |
|     0.55 | BO     | bolivia                                      | shorte      | South America |
|     0.55 | NI     | nicaragua                                    | shorte      | North America |
|     0.55 | PL     | poland                                       | uskipme     | Europe        |
|     0.55 | AI     | anguilla                                     | uskipme     |               |
|     0.55 | AS     | american samoa                               | uskipme     |               |
|     0.55 | RU     | russian federation                           | uskipme     | Europe        |
|     0.55 | CF     | central african republic                     | uskipme     | Africa        |
|     0.55 | TC     | turks and caicos islands                     | uskipme     |               |
|     0.55 | BM     | bermuda                                      | uskipme     |               |
|     0.55 | CK     | cook islands                                 | uskipme     |               |
|     0.55 | JP     | japan                                        | uskipme     | Asia          |
|     0.55 | HU     | hungary                                      | uskipme     | Europe        |
|     0.55 | EG     | egypt                                        | uskipme     | Africa        |
|     0.55 | GS     | south georgia and the south sandwich islands | uskipme     |               |
|     0.55 | GH     | ghana                                        | uskipme     | Africa        |
|     0.55 | NE     | niger                                        | uskipme     | Africa        |
|     0.54 | GD     | Grenada                                      | bcvc        | North America |
|     0.53 | LC     | Saint Lucia                                  | bcvc        | North America |
|     0.53 | NA     | Namibia                                      | bcvc        | Africa        |
|     0.53 | SV     | el salvador                                  | shorte      | North America |
|     0.52 | UY     | uruguay                                      | shorte      | South America |
|     0.51 | AI     | Anguilla                                     | bcvc        |               |
|     0.51 | TG     | Togo                                         | bcvc        | Africa        |
|     0.51 | TM     | Turkmenistan                                 | bcvc        | Asia          |
|     0.51 | YE     | yemen                                        | shorte      | Asia          |
|     0.5  | TC     | Turks And Caicos Islands                     | bcvc        |               |
|     0.5  | SZ     | Swaziland                                    | bcvc        | Africa        |
|     0.5  | BT     | Bhutan                                       | bcvc        | Asia          |
|     0.5  | VU     | Vanuatu                                      | bcvc        | Oceania       |
|     0.5  | CG     | Congo                                        | bcvc        | Africa        |
|     0.5  | LR     | Liberia                                      | bcvc        | Africa        |
|     0.5  | GQ     | Equatorial Guinea                            | bcvc        | Africa        |
|     0.5  | NE     | Niger                                        | bcvc        | Africa        |
|     0.5  | GN     | Guinea                                       | bcvc        | Africa        |
|     0.5  | YT     | Mayotte                                      | bcvc        |               |
|     0.5  | LS     | Lesotho                                      | bcvc        | Africa        |
|     0.5  | SL     | Sierra Leone                                 | bcvc        | Africa        |
|     0.5  | IS     | saint helena                                 | shorte      |               |
|     0.5  | NF     | norfolk island                               | shorte      |               |
|     0.5  | IO     | british indian ocean territory               | shorte      |               |
|     0.5  | NR     | nauru                                        | shorte      | Oceania       |
|     0.5  | AQ     | antarctica                                   | shorte      |               |
|     0.5  | PK     | pakistan                                     | uskipme     | Asia          |
|     0.5  | CX     | christmas island                             | uskipme     |               |
|     0.48 | GW     | guinea-bissau                                | shorte      | Africa        |
|     0.48 | PS     | palestine, state of                          | uskipme     |               |
|     0.48 | EC     | ecuador                                      | uskipme     | South America |
|     0.48 | VN     | viet nam                                     | uskipme     | Asia          |
|     0.48 | AL     | albania                                      | uskipme     | Europe        |
|     0.48 | GR     | greece                                       | uskipme     | Europe        |
|     0.48 | DZ     | algeria                                      | uskipme     | Africa        |
|     0.48 | MA     | morocco                                      | uskipme     | Africa        |
|     0.48 | TW     | taiwan, province of china                    | uskipme     |               |
|     0.48 | YE     | yemen                                        | uskipme     | Asia          |
|     0.46 | AF     | Afghanistan                                  | bcvc        | Asia          |
|     0.44 | SX     | sint maarten (dutch part)                    | uskipme     |               |
|     0.42 | SJ     | svalbard and jan mayen                       | uskipme     |               |
|     0.42 | VG     | virgin islands, british                      | uskipme     |               |
|     0.42 | UM     | united states minor outlying islands         | uskipme     |               |
|     0.42 | PN     | pitcairn                                     | uskipme     |               |
|     0.42 | EH     | western sahara                               | uskipme     |               |
|     0.42 | TK     | tokelau                                      | uskipme     |               |
|     0.42 | BV     | bouvet island                                | uskipme     |               |
|     0.42 | BQ     | bonaire, sint eustatius and saba             | uskipme     |               |
|     0.42 | MM     | myanmar                                      | uskipme     | Asia          |
|     0.42 | VA     | holy see (vatican city state)                | uskipme     | Europe        |
|     0.42 | DM     | dominica                                     | uskipme     | North America |
|     0.42 | IM     | isle of man                                  | uskipme     |               |
|     0.42 | IL     | israel                                       | uskipme     | Asia          |
|     0.42 | IL     | cura                                         | uskipme     |               |
|     0.42 | JE     | jersey                                       | uskipme     |               |
|     0.42 | CC     | cocos (keeling) islands                      | uskipme     |               |
|     0.42 | SS     | south sudan                                  | uskipme     |               |
|     0.42 | ER     | eritrea                                      | uskipme     | Africa        |
|     0.42 | HM     | heard island and mcdonald islands            | uskipme     |               |
|     0.42 | HM     | saint barthelemy                             | uskipme     |               |
|     0.42 | TF     | french southern territories                  | uskipme     |               |
|     0.42 | SH     | saint helena, ascension and tristan da cunha | uskipme     |               |
|     0.42 | FK     | falkland islands (malvinas)                  | uskipme     |               |
|     0.42 | PM     | saint pierre and miquelon                    | uskipme     |               |
|     0.42 | AQ     | antarctica                                   | uskipme     |               |
|     0.42 | WS     | samoa                                        | uskipme     | Oceania       |
|     0.42 | MV     | maldives                                     | uskipme     | Asia          |
|     0.42 | GG     | guernsey                                     | uskipme     |               |
|     0.41 | AF     | Cape Verde                                   | bcvc        |               |
|     0.4  | FM     | Micronesia, Federated States Of              | bcvc        | Oceania       |
|     0.34 | RW     | Rwanda                                       | bcvc        | Africa        |
|     0.29 | RW     | Proxy Traffic Deal                           | bcvc        |               |
|     0.27 | TJ     | Tajikistan                                   | bcvc        | Asia          |
|     0.15 | GG     | anonymous                                    | uskipme     |               |
|     0.1  | TV     | Tuvalu                                       | bcvc        | Oceania       |
|     0.1  | VA     | Holy See (Vatican City State)                | bcvc        | Europe        |
|     0.1  | VA     | Wallis And Futuna Islands                    | bcvc        |               |
|     0.1  | VA     | Macau                                        | bcvc        |               |
|     0.1  | VA     | Heard And Mc Donald Islands                  | bcvc        |               |
|     0.1  | NR     | Nauru                                        | bcvc        | Oceania       |
|     0.1  | AQ     | Antarctica                                   | bcvc        |               |
|     0.1  | EH     | Western Sahara                               | bcvc        |               |
|     0.1  | KR     | Korea, Republic Of                           | bcvc        | Asia          |
|     0.1  | KR     | Yugoslavia                                   | bcvc        |               |
|     0.1  | UM     | United States Minor Outlying Islands         | bcvc        |               |
|     0.1  | MH     | Marshall Islands                             | bcvc        | Oceania       |
|     0.1  | MH     | All other countries                          | bcvc        |               |
|     0.1  | TO     | Tonga                                        | bcvc        | Oceania       |
|     0.1  | IO     | British Indian Ocean Territory               | bcvc        |               |
|     0.1  | IO     | St. Helena                                   | bcvc        |               |
|     0.1  | BV     | Bouvet Island                                | bcvc        |               |
|     0.1  | CX     | Christmas Island                             | bcvc        |               |
|     0.1  | TD     | Chad                                         | bcvc        | Africa        |
|     0.1  | TD     | South Georgia, South Sandwich Islands        | bcvc        |               |
|     0.1  | CF     | Central African Republic                     | bcvc        | Africa        |
|     0.1  | BI     | Burundi                                      | bcvc        | Africa        |
|     0.1  | SO     | Somalia                                      | bcvc        | Africa        |
|     0.1  | ST     | Sao Tome And Principe                        | bcvc        | Africa        |
|     0.1  | ST     | Svalbard And Jan Mayen Islands               | bcvc        |               |
|     0.1  | TF     | French Southern Territories                  | bcvc        |               |
|     0.1  | TK     | Tokelau                                      | bcvc        |               |
|     0.1  | PW     | Palau                                        | bcvc        | Oceania       |
|     0.1  | PN     | Pitcairn                                     | bcvc        |               |
|     0.1  | PN     | France, Metropolitan                         | bcvc        |               |
|     0.1  | CC     | Cocos (Keeling) Islands                      | bcvc        |               |
|     0.1  | CK     | Cook Islands                                 | bcvc        |               |
|     0.1  | ER     | Eritrea                                      | bcvc        | Africa        |
|     0.1  | GW     | Guinea-Bissau                                | bcvc        | Africa        |
|     0.02 | KI     | caribbean netherlands                        | shorte      |               |
|     0.02 | SJ     | svalbard and jan mayen                       | shorte      |               |
|     0.02 | CC     | cocos (keeling) islands                      | shorte      |               |
|     0.02 | SS     | south sudan                                  | shorte      |               |
|     0.02 | SS     | congo - kinshasa                             | shorte      |               |
|     0.02 | SS     | sint maarten                                 | shorte      |               |
|     0.02 | SS     | curaao                                       | shorte      |               |
|     0.02 | CX     | christmas island                             | shorte      |               |
|     0.02 | PM     | saint pierre and miquelon                    | shorte      |               |
|     0    | CN     | China                                        | bcvc        | Asia          |
