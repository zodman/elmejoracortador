from webscraping import download, xpath
URL = "https://shorte.st/es/payoutrates"

def scrape_rates():
    D = download.Download()
    response = D.get(URL)
    tds = xpath.search(response,"//table")
    countries = []
    for i in tds:
        res = xpath.Doc(i).search("//tr/td")
        for i in xrange(0, len(res),2):
            co, cc = res[i].lower().strip(), res[i+1].strip()
            countries.append((co, None,cc))
    return countries

if __name__== "__main__":
    print scrape_rates()
