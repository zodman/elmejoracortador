|   Precio | code   | Pais                              | Acortador   | continent     |
|---------:|:-------|:----------------------------------|:------------|:--------------|
|     5    | US     | United States                     | bcvc        | North America |
|     4.1  | US     | united states                     | ouoio       | North America |
|     4.05 | US     | united states                     | shinkin     | North America |
|     3.5  | US     | united states                     | uskipme     | North America |
|     3.35 | CA     | canada                            | shinkin     | North America |
|     3.27 | CA     | canada                            | ouoio       | North America |
|     3.2  | CA     | Canada                            | bcvc        | North America |
|     2    | SV     | El Salvador                       | bcvc        | North America |
|     2    | PY     | Paraguay                          | bcvc        | South America |
|     1.7  | CA     | canada                            | uskipme     | North America |
|     1.65 | BR     | brazil                            | ouoio       | South America |
|     1.65 | BR     | brazil                            | shinkin     | South America |
|     1.6  | PA     | panama                            | uskipme     | North America |
|     1.5  | VE     | Venezuela                         | bcvc        | South America |
|     1.5  | PE     | peru                              | shinkin     | South America |
|     1.5  | PA     | panama                            | shinkin     | North America |
|     1.5  | PY     | paraguay                          | shinkin     | South America |
|     1.5  | MX     | mexico                            | shinkin     | North America |
|     1.5  | NI     | nicaragua                         | shinkin     | North America |
|     1.5  | GY     | guyana                            | shinkin     | South America |
|     1.5  | TT     | trinidad and tobago               | shinkin     | North America |
|     1.5  | UY     | uruguay                           | shinkin     | South America |
|     1.5  | VE     | venezuela                         | shinkin     | South America |
|     1.5  | VC     | saint vincent and the grenadines  | shinkin     | North America |
|     1.5  | LC     | saint lucia                       | shinkin     | North America |
|     1.5  | SR     | suriname                          | shinkin     | South America |
|     1.5  | KN     | saint kitts and nevis             | shinkin     | North America |
|     1.5  | EC     | ecuador                           | shinkin     | South America |
|     1.5  | CO     | colombia                          | shinkin     | South America |
|     1.5  | CL     | chile                             | shinkin     | South America |
|     1.5  | DO     | dominican republic                | shinkin     | North America |
|     1.5  | DM     | dominica                          | shinkin     | North America |
|     1.5  | CU     | cuba                              | shinkin     | North America |
|     1.5  | CR     | costa rica                        | shinkin     | North America |
|     1.5  | AR     | argentina                         | shinkin     | South America |
|     1.5  | AG     | antigua and barbuda               | shinkin     | North America |
|     1.5  | BS     | bahamas                           | shinkin     | North America |
|     1.5  | BO     | bolivia                           | shinkin     | South America |
|     1.5  | BZ     | belize                            | shinkin     | North America |
|     1.5  | BB     | barbados                          | shinkin     | North America |
|     1.5  | JM     | jamaica                           | shinkin     | North America |
|     1.5  | HN     | honduras                          | shinkin     | North America |
|     1.5  | SV     | el salvador                       | shinkin     | North America |
|     1.5  | GT     | guatemala                         | shinkin     | North America |
|     1.5  | GD     | grenada                           | shinkin     | North America |
|     1.44 | KN     | saint kitts and nevis             | shorte      | North America |
|     1.42 | BB     | barbados                          | shorte      | North America |
|     1.37 | BS     | bahamas                           | shorte      | North America |
|     1.31 | DM     | dominica                          | shorte      | North America |
|     1.3  | BR     | brazil                            | uskipme     | South America |
|     1.26 | LC     | saint lucia                       | shorte      | North America |
|     1.25 | AG     | antigua and barbuda               | shorte      | North America |
|     1.2  | BR     | Brazil                            | bcvc        | South America |
|     1.2  | EC     | Ecuador                           | bcvc        | South America |
|     1.2  | MX     | mexico                            | uskipme     | North America |
|     1.14 | TT     | trinidad and tobago               | shorte      | North America |
|     1.14 | DO     | dominican republic                | shorte      | North America |
|     1.1  | CO     | colombia                          | uskipme     | South America |
|     1.1  | CL     | chile                             | uskipme     | South America |
|     1.1  | AR     | argentina                         | uskipme     | South America |
|     1.1  | VE     | venezuela, bolivarian republic of | uskipme     | South America |
|     1.02 | PA     | panama                            | shorte      | North America |
|     1.01 | EC     | ecuador                           | shorte      | South America |
|     1    | UY     | Uruguay                           | bcvc        | South America |
|     1    | MX     | Mexico                            | bcvc        | North America |
|     1    | CL     | Chile                             | bcvc        | South America |
|     1    | PE     | Peru                              | bcvc        | South America |
|     1    | SV     | el salvador                       | uskipme     | North America |
|     1    | CU     | cuba                              | uskipme     | North America |
|     0.98 | GY     | guyana                            | shorte      | South America |
|     0.98 | CU     | cuba                              | shorte      | North America |
|     0.95 | CR     | costa rica                        | uskipme     | North America |
|     0.93 | KN     | Saint Kitts And Nevis             | bcvc        | North America |
|     0.91 | HN     | Honduras                          | bcvc        | North America |
|     0.88 | CR     | Costa Rica                        | bcvc        | North America |
|     0.88 | CU     | Cuba                              | bcvc        | North America |
|     0.88 | VC     | Saint Vincent And The Grenadines  | bcvc        | North America |
|     0.88 | GD     | grenada                           | shorte      | North America |
|     0.88 | BZ     | belize                            | shorte      | North America |
|     0.86 | CR     | costa rica                        | shorte      | North America |
|     0.86 | SR     | suriname                          | shorte      | South America |
|     0.85 | BO     | bolivia, plurinational state of   | uskipme     | South America |
|     0.85 | HN     | honduras                          | uskipme     | North America |
|     0.81 | BB     | Barbados                          | bcvc        | North America |
|     0.81 | CL     | chile                             | shorte      | South America |
|     0.8  | CO     | Colombia                          | bcvc        | South America |
|     0.8  | AR     | Argentina                         | bcvc        | South America |
|     0.8  | BO     | Bolivia                           | bcvc        | South America |
|     0.8  | JM     | jamaica                           | uskipme     | North America |
|     0.79 | JM     | Jamaica                           | bcvc        | North America |
|     0.78 | PA     | Panama                            | bcvc        | North America |
|     0.77 | GT     | Guatemala                         | bcvc        | North America |
|     0.77 | NI     | Nicaragua                         | bcvc        | North America |
|     0.77 | DO     | Dominican Republic                | bcvc        | North America |
|     0.75 | TT     | Trinidad And Tobago               | bcvc        | North America |
|     0.75 | PY     | paraguay                          | uskipme     | South America |
|     0.75 | UY     | uruguay                           | uskipme     | South America |
|     0.75 | GT     | guatemala                         | uskipme     | North America |
|     0.72 | BB     | barbados                          | uskipme     | North America |
|     0.71 | BS     | Bahamas                           | bcvc        | North America |
|     0.7  | TT     | trinidad and tobago               | uskipme     | North America |
|     0.7  | GY     | guyana                            | uskipme     | South America |
|     0.7  | DO     | dominican republic                | uskipme     | North America |
|     0.7  | BZ     | belize                            | uskipme     | North America |
|     0.7  | BS     | bahamas                           | uskipme     | North America |
|     0.7  | GD     | grenada                           | uskipme     | North America |
|     0.7  | NI     | nicaragua                         | uskipme     | North America |
|     0.7  | VC     | saint vincent and the grenadines  | uskipme     | North America |
|     0.7  | SR     | suriname                          | uskipme     | South America |
|     0.69 | AG     | Antigua And Barbuda               | bcvc        | North America |
|     0.67 | BZ     | Belize                            | bcvc        | North America |
|     0.66 | PE     | peru                              | shorte      | South America |
|     0.65 | GY     | Guyana                            | bcvc        | South America |
|     0.65 | PE     | peru                              | uskipme     | South America |
|     0.65 | LC     | saint lucia                       | uskipme     | North America |
|     0.65 | KN     | saint kitts and nevis             | uskipme     | North America |
|     0.65 | AG     | antigua and barbuda               | uskipme     | North America |
|     0.64 | AR     | argentina                         | shorte      | South America |
|     0.64 | GT     | guatemala                         | shorte      | North America |
|     0.6  | DM     | Dominica                          | bcvc        | North America |
|     0.6  | PY     | paraguay                          | shorte      | South America |
|     0.6  | JM     | jamaica                           | shorte      | North America |
|     0.6  | VE     | venezuela                         | shorte      | South America |
|     0.59 | SR     | Suriname                          | bcvc        | South America |
|     0.59 | CO     | colombia                          | shorte      | South America |
|     0.59 | HN     | honduras                          | shorte      | North America |
|     0.55 | BO     | bolivia                           | shorte      | South America |
|     0.55 | NI     | nicaragua                         | shorte      | North America |
|     0.54 | GD     | Grenada                           | bcvc        | North America |
|     0.53 | LC     | Saint Lucia                       | bcvc        | North America |
|     0.53 | SV     | el salvador                       | shorte      | North America |
|     0.52 | UY     | uruguay                           | shorte      | South America |
|     0.48 | EC     | ecuador                           | uskipme     | South America |
|     0.42 | DM     | dominica                          | uskipme     | North America |
