import Vue from 'vue/dist/vue.js';
import gpdData from 'assets/out.json';

require('jvectormap-next')($);

var document_ready = function(){

    $('#world-map').vectorMap({
        map: 'world_mill_en',
        series: {
            regions: [{
                values: gpdData,
                scale: ['#C8EEFF', '#0071A4'],
                normalizeFunction: 'polynomial'
            }]
        },
        onRegionTipShow: function(e, el, code){
            app.selected = gpdData[code];
            el.html(el.html());
        }
    });

}

$(document).ready(document_ready);

var app = new Vue({
    el: '#app',
    data(){
        return {
            selected:false
        }
    },
    methods: {
        formatPrice(value) {
            let val = (value/1).toFixed(2).replace(',', '.')
            return val
        }
    }
});
