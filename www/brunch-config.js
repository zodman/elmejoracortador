// See http://brunch.io for documentation.
exports.files = {
  javascripts: {joinTo: 'app.js'},
  stylesheets: {joinTo: 'app.css'}
};
exports.plugins = {
    sass:{},
    browserSync:{
          port: 3333,
          logLevel: "debug"
    },
    babel: {
      presets: ['es2015']
    },
    copycat: {
    }
}

exports.npm = {
    globals: {
        'jQuery': "jquery",
        '$': 'jquery'
    },
    styles:{
        'jvectormap-next': ["jquery-jvectormap.css"],
        'bulma':['css/bulma.css.map','css/bulma.css']
    }
}
