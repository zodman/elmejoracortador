from webscraping import download, xpath
URL = "http://ouo.io/rates"

def scrape_rates():
    D = download.Download()
    response = D.get(URL)
    tds = xpath.search(response,"//table/tbody/tr/td")
    countries = []
    for i in xrange(0,len(tds)-1,3):
        cc,price= tds[i+1].lower(), tds[i+2]
        price = price.replace("USD ","")
        countries.append((cc, None,price))
    return countries

if __name__== "__main__":
    print scrape_rates()
