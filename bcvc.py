from webscraping import download, xpath
URL = "http://bc.vc/publishers_rates.php"

def scrape_rates():
    D = download.Download()
    response = D.get(URL)
    tds = xpath.search(response,"//table/tbody/tr/td")
    countries = []
    for i in xrange(0,len(tds),3):
        countries.append((tds[i], "$" +tds[i+1], "$"+tds[i+2]))
    return countries

if __name__== "__main__":
    print scrape_rates()
