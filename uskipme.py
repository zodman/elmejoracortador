from webscraping import download, xpath
URL = "http://www.uskip.me/rates"

def scrape_rates():
    D = download.Download()
    response = D.get(URL)
    tds = xpath.search(response,"//table/tbody/tr/td")
    countries = []
    for i in xrange(0,len(tds),2):
        co,cc= tds[i],tds[i+1].replace("USD","")
        countries.append((co.lower(), None, cc.strip()))
    return countries

if __name__== "__main__":
    print scrape_rates()
