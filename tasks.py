from invoke import task
@task
def run(ctx):
    ctx.run("python main.py")
    ctx.run("cp out.json www/app/assets/")

@task
def all(ctx):
    ctx.run("python main.py > ALL.md")
    ctx.run("python main.py --filtercontinent america > AmericaLatina.md")
